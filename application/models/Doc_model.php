<?php
    class Doc_model extends CI_Model {
        public function __construct() {
            $this->load->database();
        }

        public function get_docs() {
            $query = $this->db->get('docs');
            return $query->result_array();
        }
    }

?>
<h2><?= $title ?></h2>
<br>
<?php foreach($docs as $doc) : ?>
<div class="card border-primary mb-3" style="max-width: 100rem;">
  <div class="card-body">
    <h4 class="card-title"><?= $doc['title']; ?></h4>
    <p class="card-text"><?= $doc['body']; ?></p>
  </div>
</div>
<?php endforeach; ?>
<html>
    <head>
        <title>Test</title>
        <link rel="stylesheet" href="https://bootswatch.com/4/materia/bootstrap.min.css">
    </head>
<body>

    
<nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-5">
    <div class="container">
        <a class="navbar-brand" href="<?=base_url(); ?>">CodeIgniter</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url(); ?>">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url("/about"); ?>">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?=base_url("/docs"); ?>">Docs</a>
            </li>
            </ul>
        </div>

  </div>
</nav>

<div class="container">
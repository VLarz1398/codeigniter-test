<?php
    class Docs extends CI_Controller {
        public function index() {
            $data['title'] = 'Documentation';

            $data['docs'] = $this->doc_model->get_docs();
            // print_r($data['docs']);

            $this->load->view('includes/header');
            $this->load->view('docs/index', $data);
            $this->load->view('includes/footer');
        }
    }

?>